// @dart=2.9
import 'dart:io';
import 'dart:ui';

import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';
import 'package:flutter_file_manager/flutter_file_manager.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path_provider_ex/path_provider_ex.dart';
import 'package:share/share.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import 'package:syncfusion_flutter_signaturepad/signaturepad.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyHomePageState(); //create state
  }
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  var files;

  @override
  void initState() {
    getFiles(); //call getFiles() function on initial state.
    super.initState();
  }

  void openExistingPdf() async {
    FilePickerResult result = await FilePicker.platform.pickFiles();

    if (result != null) {
      File file = File(result.files.single.path);

      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return ViewPDF(path: file.path);
        //open viewPDF page on click
      }));
    } else {
      // User canceled the picker
    }
  }

  void getFiles() async {
    //asyn function to get list of files
    var status = await Permission.storage.status;

    if (!status.isGranted) {
      await Permission.storage.request();
    }

    List<StorageInfo> storageInfo = await PathProviderEx.getStorageInfo();
    var root = storageInfo[0]
        .rootDir;
    print(root);
    var fm = FileManager(root: Directory(root)); //
    files = await fm.filesTree(
        excludedPaths: ["/storage/emulated/0/Android"],
        extensions: ["pdf"]
        );

    setState(() {}); //update the UI
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Files'),
        ),
        /*floatingActionButton: FloatingActionButton(
          onPressed: () {
            openExistingPdf();
          },
          tooltip: 'Increment',
          child: Icon(Icons.add),
        ),*/
        body: files == null
            ? Container(
                margin: EdgeInsets.all(10),
                height: 50.0,
                decoration: BoxDecoration(
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.blue.withOpacity(0.1),
                      blurRadius: 1,
                      offset: Offset(10, 10),
                    ),
                  ],
                ),
                child: RaisedButton(
                  elevation: 30,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0.0),
                      side: BorderSide(color: Color.fromRGBO(0, 160, 227, 1))),
                  onPressed: () {
                    openExistingPdf();
                  },
                  padding: EdgeInsets.all(10.0),
                  color: Color.fromRGBO(0, 160, 227, 1),
                  textColor: Colors.white,
                  child: Text("open pdf".toUpperCase(),
                      style: TextStyle(fontSize: 15)),
                ),
              )
            : Column(children: [
                Container(
                  margin: EdgeInsets.all(10),
                  width: double.infinity,
                  height: 50.0,
                  decoration: BoxDecoration(
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        color: Colors.blue.withOpacity(0.1),
                        blurRadius: 1,
                        offset: Offset(10, 10),
                      ),
                    ],
                  ),
                  child: RaisedButton(
                    elevation: 30,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0.0),
                        side:
                            BorderSide(color: Color.fromRGBO(0, 160, 227, 1))),
                    onPressed: () {
                      openExistingPdf();
                    },
                    padding: EdgeInsets.all(10.0),
                    color: Color.fromRGBO(0, 160, 227, 1),
                    textColor: Colors.white,
                    child: Text("open pdf".toUpperCase(),
                        style: TextStyle(fontSize: 15)),
                  ),
                ),
                Expanded(
                    child: Container(
                        child: ListView.builder(
                  //if file/folder list is grabbed, then show here
                  itemCount: files?.length ?? 0,
                  itemBuilder: (context, index) {
                    return Card(
                        child: ListTile(
                      title: Text(files[index].path.split('/').last),
                      leading: Icon(Icons.picture_as_pdf),
                      trailing: Icon(
                        Icons.arrow_forward,
                        color: Colors.redAccent,
                      ),
                      onTap: () async {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return ViewPDF(path: files[index].path);
                          //open viewPDF page on click
                        }));
                      },
                    ));
                  },
                )))
              ]));
  }
}

class ViewPDF extends StatelessWidget {
  String path = "";
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();
  final PdfViewerController _pdfViewerController = new PdfViewerController();

  ViewPDF({this.path});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pdf View'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (_pdfViewerController.pageCount == 1) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ViewSignPad(path: path, curPage: 1)));
          } else {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ViewSignPad(
                          path: path,
                          curPage: _pdfViewerController.pageNumber,
                        )));
          }
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
      body: Center(
          child: SfPdfViewer.file(
        File(path),
        controller: _pdfViewerController,
        pageLayoutMode: PdfPageLayoutMode.single,
      )),
    );
  }
}

class ViewSignPad extends StatelessWidget {
  String path = "";
  int curPage = 1;
  GlobalKey<SfSignaturePadState> keySignaturePad =
      GlobalKey<SfSignaturePadState>();
  ViewSignPad({this.path, this.curPage});

  Future onSubmit(BuildContext context) async {
    final image = await keySignaturePad.currentState?.toImage();

    final imageSignature = await image.toByteData(format: ImageByteFormat.png);

    final file = await PdfApi.generatePdf(
        imageSignature: imageSignature, path: path, curNumber: curPage, context: context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SfSignaturePad(
          key: keySignaturePad,
          backgroundColor: Colors.transparent,
          strokeColor: Colors.black,
          minimumStrokeWidth: 3.0,
        ),
      ),
      bottomNavigationBar: ElevatedButton(
        /*style: ElevatedButton.styleFrom(
        minimumSize: Size(double.infinity, 50),
    ),*/
        child: Text('Submit signature'),
        onPressed: () {
          onSubmit(context);
        },
      ),
    );
  }
}

class PdfApi {
  static Future<File> saveFile(PdfDocument pdfDocument, BuildContext context) async {
    List<StorageInfo> storageInfo = await PathProviderEx.getStorageInfo();
    var path = storageInfo[0]
        .rootDir; //storageInfo[1] for SD card, geting the root directory
    final fileName = path + '/' + 'signature.pdf';
    final file = File(fileName);
    file.writeAsBytes(pdfDocument.save());
    Navigator.push(context,
        MaterialPageRoute(builder: (context) {
          return DocumentPreview(fileName);
          //open viewPDF page on click
        }));
    pdfDocument.dispose();
  }

  static Future<File> generatePdf(
      {ByteData imageSignature, String path, int curNumber, BuildContext context}) async {
    PdfDocument pdfDocument =
        PdfDocument(inputBytes: File(path).readAsBytesSync());
    PdfPage pdfPage = pdfDocument.pages[curNumber];
    PdfSignatureField field = pdfDocument.form.fields[1] as PdfSignatureField;

    final ByteData data = await rootBundle
        .load('assets/pdf/certificate.pfx');

//Creates a digital signature.
    field.signature = PdfSignature(
        //Creates a certificate instance from the PFX file with a private key.
        certificate: PdfCertificate(
            data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes),
            'password123'),
        digestAlgorithm: DigestAlgorithm.sha512,
        cryptographicStandard: CryptographicStandard.cms);

    final pageSize = pdfPage.getClientSize();
    final PdfBitmap image = PdfBitmap(imageSignature.buffer.asUint8List());
    pdfPage.graphics.drawImage(image,
        Rect.fromLTWH(pageSize.width - 120, pageSize.height - 200, 100, 40));
    return saveFile(pdfDocument, context);
  }
}

class DocumentPreview extends StatelessWidget {
  final String pdfPath;

  DocumentPreview(this.pdfPath);

  @override
  Widget build(BuildContext context) {
    return PDFViewerScaffold(
        appBar: AppBar(
          title: Text("Document"),
          actions: [
            Padding(
                padding: EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Share.shareFiles([pdfPath],
                        text: 'My signature document');
                  },
                  child: Icon(
                    Icons.share,
                    size: 26.0,
                  ),
                )),
          ],
        ),
        path: pdfPath);
  }
}
